! ==================== !
! Programming Practice !
! ==================== !
! Venkata Krisshna

! Climbing stairs
! Algorithms For Interviews - Problem 9.2, page 74

program climbing_stairs
  implicit none
  integer :: n,total
  integer,dimension(:),allocatable :: step

  print*,'======================================'
  print*,'Programming Practice - Climbing stairs'
  print*,'======================================'
  call init
  call climb

contains

  ! Read input
  subroutine init
    implicit none

    open(21, file="input")
    read(21,*) total   ! Total number of steps
    read(21,*) n       ! Number of step sizes
    allocate(step(n))
    read(21,*) step    ! Step sizes

    return
  end subroutine init

  ! Climb the staircase
  subroutine climb
    implicit none
    integer np,nt,sn,k

    np = total
    nt = 0
    sn = 1
    main: do
       if(np.ge.1) then
          np = np-2
          nt = nt+1
          if (np.eq.0) then
             sn = sn+1
             exit main
          end if
          if (np.eq.-1) exit main
          sn = sn + (factorial(nt+np)/((factorial(np))*(factorial(nt))))
       end if
    end do main

    print*,'For N = ',total,', there are',sn,'ways to climb the stairs.'

    return
  end subroutine climb
  
  ! Function to compute factorial
  recursive function factorial(nfact)  result(fact)
    implicit none
    integer :: fact
    integer, intent(in) :: nfact

    if (nfact == 0) then
       fact = 1
    else
       fact = nfact * factorial(nfact-1)
    end if

  end function factorial

end program climbing_stairs
